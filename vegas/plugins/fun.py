import json
import random
import discord
import asyncio
from discord.ext import commands

from .base import BasePlugin

# All plugins should inherit form BasePlugin
class FunPlugin(BasePlugin):
    # If you want to use sub commands you must use this decorator to create a group
    @commands.command(aliases=['8ball'])
    async def _8ball(self, ctx, *, question=None):
        responses = ["Ask again when I give a shit.",
        "Yes, if you leave me alone.",
        "Coward.",
        "Well, duh.",
        "Try again when I actually care.",
        "Cannot predict now, I'm sleepy.",
        "Actually think about what you are asking me and try again.",
        "My sources say no.",
        "No...just no.",
        "`[In energy saving mode. Try again never]`",
        "Yeah, Sure.",
        "Better not tell you now.",
        "Concentrate and ask again.",
        "Don't count on it.",
        "Outlook not so good.",
        "Very doubtful.",
        "Yes.",
        "Absolutely.",
        "Whatever."]
        if question:
            if 'are you working?' in question:
                await  ctx.send('nope, try again later')
            elif 'still dead?' in question:
                await  ctx.send('indeed i am, now go ahead with your day, i aint working')
            else:
                await ctx.send(f'question: {question}\nAnswer: {random.choice(responses)}')

        elif not question:
            await  ctx.send('hey bud, i cant answer a empty question, speak up')

    @commands.command()
    async def asynciotest(self, ctx):
        await ctx.send('what the fuck now')
        await asyncio.sleep(5)
        await ctx.send('oh so you\'re not gonna respond?')



def setup(bot):
    bot.add_cog(FunPlugin(bot))
